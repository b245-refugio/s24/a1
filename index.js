let getCube = Math.pow(5,3);
console.log(`The cube of 5 is ${getCube}.`);

// [ houseNumber, street, barangay, municipality, Province, zipCode ]
let address = [ 111 , "Kaliwa", "Ainagbuhatan", "Alugbati", "Capiz", 1234 ]

let [hNumber,stName,brgyName,muniName,provName,zipCode] = address;

console.log(`I live at ${hNumber} ${stName} ${brgyName} ${muniName} ${provName} ${zipCode}.`);

// { name, species, weight, measurement }

let animal = {
	name: "Miraidon",
	species: "Paradox Pokémon",
	weight: "529.1 lbs",
	measurement: "3.5 m"
}

let {name,species,weight,measurement} = animal;
console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

let number = [1,2,3,4,5];

number.forEach((number) => console.log(number));

let reduceNumber = number.reduce((x,y) => (x + y));
console.log(reduceNumber);

// Pwede din kasi itong with curly brackets hehe pwede din yung single line try nio po comment out nalang if want to try
/*	let reduceNumber = number.reduce((x,y) => {
		return x + y
	});
	console.log(reduceNumber);*/

class Dog{
	constructor(name,age,breed){
		this.dogName = name;
		this.dogAge = age;
		this.dogBreed = breed;

	}
}

let dog = new Dog("Scoobie Doo", 4, "German Shepered");
console.log(dog);